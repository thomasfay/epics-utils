import argparse

# input file name
# replace in-place

parser = argparse.ArgumentParser()
parser.add_argument('inputFile', help = 'Input file for addition of DESCRIPTION info tags.')
parser.add_argument('system', help = 'Value for SYSTEM tag.')
parser.add_argument('--removeTags', nargs='?', const = True, default = None, help = 'Remove any info tags present in the file.')

args = parser.parse_args()
inputFile = args.inputFile
removeTags = args.removeTags
system = args.system
matchTerm = "^}"
matchSys="info(SYSTEM,"
insertTag="    info(SYSTEM,	" + system + ")\n"

strOut=''

with open(inputFile, 'r') as readFile:
    for line in readFile:
        if removeTags is not None:
            if matchSys not in line:
                strOut += line 
        if line[0] == "}" and removeTags is None:
            strOut += insertTag 
            strOut += line
        else:
            strOut += line


with open(inputFile, 'w') as updateFile:
    updateFile.write(strOut)
