#!/bin/env python3
import sys
LISTFILE=sys.argv[1]

with open(LISTFILE) as f:
    for l in f.readlines():
        pvname = l.split("\n")[0]
        if pvname.count(':') == 2:
            prop = pvname.split(":")[-1]
            prop = prop.replace(" ","")
            propInfo = str(len(prop)) 
        else:
            propInfo = "Invalid PV name"
        print("%s _%s" % (pvname, propInfo)) 
