import argparse

# input file name
# replace in-place

parser = argparse.ArgumentParser()
parser.add_argument('inputFile', help = 'Input file for addition of DESCRIPTION info tags.')
parser.add_argument('--removeTags', nargs='?', const = True, default = None, help = 'Remove any info tags present in the file.')

args = parser.parse_args()
inputFile = args.inputFile
removeTags = args.removeTags

matchDESC="field(DESC,"
insertTag="info(DESCRIPTION,"


strOut=''

with open(inputFile, 'r') as readFile:
    for line in readFile:
        if removeTags is not None:
            if insertTag not in line:
                strOut += line 
        else:
            strOut += line
        if matchDESC in line and removeTags is None:
            strOut += line.replace(matchDESC,insertTag)


with open(inputFile, 'w') as updateFile:
    updateFile.write(strOut)
